package jcl.test.mvp.presenter;

import java.util.List;

import jcl.test.mvp.R;
import jcl.test.mvp.model.DBManager;
import jcl.test.mvp.model.User;
import jcl.test.mvp.model.UserDao;
import jcl.test.mvp.view.LoginView;

/**
 * Created by jayanthony.lumba on 4/21/2016.
 */
public class LoginPresenter {

    LoginView view;

    public LoginPresenter(LoginView view) {
        this.view = view;
    }

    public void validateCredentials() {
        //Check required fields
        if (isUsernameEmpty()!= true && isPasswordEmpty() != true) {
            //Instantiate user table
            UserDao userTable = DBManager.newSession().getUserDao();
            //Select users base on username and password using where condition
            List<User> users = userTable
                    .queryBuilder()
                    .where(UserDao.Properties.Username.eq(view.getUsername()),
                            UserDao.Properties.Password.eq(view.getPassword()))
                    .list();
            if (users.size() > 0){
                view.showMessage(R.string.message_login_success);
            }else{
                view.showMessage(R.string.message_login_failed);
            }
        }

    }

    public boolean isUsernameEmpty(){
         if(view.getUsername().isEmpty()){
            view.setUsernameError();
             return true;
         }
        return false;
    }

    public boolean isPasswordEmpty(){
        if(view.getPassword().isEmpty()){
            view.setPasswordError();
            return true;
        }
        return false;
    }

}
