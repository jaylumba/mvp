package jcl.test.mvp.view;

/**
 * Created by jayanthony.lumba on 4/21/2016.
 */
public interface LoginView {

    public String getUsername();
    public String getPassword();
    public void setUsernameError();
    public void setPasswordError();
    public void showMessage(int message);
    public void onLoginEvent();

}
