package jcl.test.mvp.view;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jcl.test.mvp.R;
import jcl.test.mvp.model.DBManager;
import jcl.test.mvp.model.User;
import jcl.test.mvp.model.UserDao;
import jcl.test.mvp.presenter.LoginPresenter;
import jcl.test.mvp.view.LoginView;

public class LoginActivity extends AppCompatActivity implements LoginView {

    @Bind(R.id.txt_username)
    EditText txt_username;
    @Bind(R.id.txt_password)
    EditText txt_password;

    LoginPresenter presenter;

    Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        presenter = new LoginPresenter(this);

        //TODO Uncomment on first run
        //initUsersonDB();
    }

    private void initUsersonDB(){
        //Instantiate the table
        UserDao userDao = DBManager.newSession().getUserDao();
        //Create users
        User user1 = new User();
        user1.setUsername("user1");
        user1.setPassword("pass1");

        User user2 = new User();
        user2.setUsername("user2");
        user2.setPassword("pass2");

        //Insert users
        userDao.insert(user1);
        userDao.insert(user2);
    }

    @Override
    public String getUsername() {
        return txt_username.getText().toString();
    }

    @Override
    public String getPassword() {
        return txt_password.getText().toString();
    }

    @Override
    public void setUsernameError() {
        txt_username.setError(getString(R.string.message_error_requiredfield));
    }

    @Override
    public void setPasswordError() {
        txt_password.setError(getString(R.string.message_error_requiredfield));
    }

    @Override
    public void showMessage(int resId) {
        if(toast != null){
            toast.cancel();
        }
        toast = Toast.makeText(getApplicationContext(), getStringFromResource(resId), Toast.LENGTH_SHORT);
        toast.show();
    }

    @OnClick(R.id.btn_login)
    @Override
    public void onLoginEvent() {
        presenter.validateCredentials();
    }

    public String getStringFromResource(int resId){
        return getString(resId);
    }
}
