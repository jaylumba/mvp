package jcl.test.mvp;

import android.app.Application;

import jcl.test.mvp.model.DBManager;

/**
 * Created by jayanthony.lumba on 3/3/2016.
 */
public class App extends Application{

    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        this.instance = this;
        DBManager.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        DBManager.destroy();
    }

    public static App getInstance() {
        return instance;
    }
}
