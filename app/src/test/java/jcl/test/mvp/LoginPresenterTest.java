package jcl.test.mvp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import jcl.test.mvp.model.DBManager;
import jcl.test.mvp.model.User;
import jcl.test.mvp.presenter.LoginPresenter;
import jcl.test.mvp.view.LoginView;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jayanthony.lumba on 4/25/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest {

    @Mock
    private LoginView view;

    private LoginPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new LoginPresenter(view);
    }

    @Test
    public void shouldShowErrorMessageWhenUsernameIsEmpty() throws Exception {
        when(view.getUsername()).thenReturn("");
        presenter.isUsernameEmpty();
        verify(view).setUsernameError();
    }

    @Test
    public void shouldShowErrorMessageWhenPasswordIsEmpty() throws Exception {
        when(view.getPassword()).thenReturn("");
        presenter.isPasswordEmpty();
        verify(view).setPasswordError();
    }

    @Test
    public void shouldShowSuccessMessageWhenLogin() throws Exception{

        List<User> users = new ArrayList<User>();
        User user = new User();
        user.setId((long)1);
        user.setUsername("user1");
        user.setPassword("pass1");
        users.add(user);

        // create and configure mock
        App app = Mockito.mock(App.class);
        DBManager dbManager = Mockito.mock(DBManager.class);
        dbManager.init(app);
//        when(test.getUserByUsernameAndPassword(view.getUsername(),view.getPassword())).thenReturn(users);


        when(view.getUsername()).thenReturn("user1");
        when(view.getPassword()).thenReturn("pass1");
        presenter.validateCredentials();
        verify(view).showMessage(R.string.message_login_success);
    }

}
