package com.daogenerator;

import android.app.Application;

public class DBManager {
	private final static String DB_NAME = "databasename.db";
	private DBManager() {}

	private static DaoMaster.DevOpenHelper sDevOpenHelper;
	private static DaoMaster sDaoMaster;

	public static void init(Application app) {
		sDevOpenHelper = new DaoMaster.DevOpenHelper(app, DB_NAME, null);
		sDaoMaster = new DaoMaster(sDevOpenHelper.getWritableDatabase());
	}

	public static void destroy() {
		try {
			if (sDaoMaster != null) {
				sDaoMaster.getDatabase().close();
				sDaoMaster = null;
			}

			if (sDevOpenHelper != null) {
				sDevOpenHelper.close();
				sDevOpenHelper = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static DaoSession newSession() {
		if (sDaoMaster == null) {
			throw new RuntimeException("sDaoMaster is null.");
		}
		return sDaoMaster.newSession();
	}

}
