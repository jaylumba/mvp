package com.daogenerator;


import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    private static final String PROJECT_DIR = System.getProperty("user.dir");
    private static final String destinationPackage = "jcl.test.mvp.databases.gen";

    public static void main(String[] args) {
        Schema schema = new Schema(1, destinationPackage);
        schema.enableKeepSectionsByDefault();
        addTables(schema);
        try {
            new DaoGenerator().generateAll(schema, PROJECT_DIR + "/app/src/main/java");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addTables(final Schema schema) {
        addUser(schema);
    }

    private static Entity addUser(final Schema schema) {
        Entity user = schema.addEntity("User");
        user.addIdProperty().primaryKey().autoincrement();
        user.addStringProperty("username").notNull();
        user.addStringProperty("password").notNull();
        return user;
    }

    /****************************************
     ***CREATE CLASS named DBManager.java****
     ****************************************/
    // Steps //
    // 1. create new class on your project package
    // 2. Copy the code on DBManager.txt to your new class.
    // 3. Change the package name accordingly.


    /****************************************
     *******CREATE CLASS named App.java******
     ****************************************/
    // Steps //
    // 1. create new class on your project package
    // 2. Copy the code on App.txt to your new class.
    // 3. Change the package name accordingly.
    // 4. Go to your manifest and on the application tag
    //    add the below code:
    //    android:name=".App"
    //Example:
    //<application
    //    android:name=".App"
    //    android:allowBackup="true"
    //    android:icon="@mipmap/ic_launcher">

    //Sample usage
    // INSERT USER ON DB
    //    UserDao userDao = DBManager.newSession().getUserDao();
    //    User user = new User();
    //    user.setUsername("user1");
    //    user.setPassword(("myP@ssw0rd");
    //    userDao.insert(user);

}
